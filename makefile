# If the first argument is "exec"...
ifeq (exec,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "exec"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

# make for stop-start docker-compose stack

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

up: ## start or restart compose stack
	docker-compose up -d

stop: ## stop the stack
	docker-compose down

logs: ## follow logs
	docker-compose logs -f

ps: ## list container and status
	docker-compose ps

exec: ## go into a container
	docker-compose exec $(RUN_ARGS) sh

ssh: ## go into a container
	docker-compose exec $(RUN_ARGS) sh

